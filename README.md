# mrlc.dev

Here is the publicly-accessible source code for my personal technical website, [mrlc.dev](https://mrlc.dev). I am using [Hugo](https://gohugo.io) as the static site generator framework, with [this](https://github.com/panr/hugo-theme-terminal) terminal theme.
