---
title: "About"
layout: "single"
#date: 2021-01-08T05:12:23Z
---

Welcome to `mrlc.dev`. My name is Max, and I am a college student studying computer science who enjoys tinkering with Linux and programming in his spare time.

On this website, I post technical tutorials and do rundowns of various projects which I have done. My other website, [`maxchristman.com`](https://maxchristman.com), will have non-technical content, but currently links back here.

This website was created with [Hugo](https://gohugo.io) with the [terminal](https://github.com/panr/hugo-theme-terminal) theme. The [source code for this](https://gitlab.com/maxchristman/mrlc.dev) and my other projects are licensed under the GPLv3 and available on my [GitLab](https://gitlab.com/maxchristman) and my [GitHub](https://github.com/maxchristman).
